# Changelog

## 4.0.0 - 2022-08-09
* Removing `php_unit_test_class_requires_covers` which does not recognize `CoversClass` attribute

## 3.2.0 - 2022-05-29
* Adding PHPUnit rules

## 3.1.0 - 2022-05-13
* Amending `types_spaces` to only add spaces around pipes in multi-catch clauses
